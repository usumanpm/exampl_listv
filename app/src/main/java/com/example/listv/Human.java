package com.example.listv;

public class Human {
    private String fami;
    private String name;
    private String otch;
    private int phot;
    public Human(String fam,String nam,String otc,int pho){
        this.fami=fam;
        this.name=nam;
        this.otch=otc;
        this.phot=pho;
    }
    public String getFami(){
        return this.fami;
    }

    public void setFami(String fami) {
        this.fami = fami;
    }
    public String getName(){
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getOtch(){
        return this.otch;
    }
    public void setOtch(String otch) {
        this.otch = otch;
    }
    public int getPhot() {
        return this.phot;
    }

    public void setPhot(int phot) {
        this.phot = phot;
    }
}
