package com.example.listv;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    ArrayList<Human> hum=new ArrayList<Human>();
    ListView lv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setInitialDate();
        lv = findViewById(R.id.listv);
        StateAdapter stateAdapter=new StateAdapter(this,R.layout.forlistv,hum);
        lv.setAdapter(stateAdapter);

    }
    void setInitialDate(){
        hum.add(new Human("Николаев", "Анатолий","Николаевич",R.drawable.phot1));
        hum.add(new Human("Иванов", "Иван","Иванович",R.drawable.phot2));
        hum.add(new Human("Иванов", "Иван","Иванович",R.drawable.phot3));

    }

}